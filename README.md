## Trip sorter strategy
 
Lib based on PHP in version 7.1.

At this moment project provides possibility to ready only from json files.
 
## Features
Extend a project to read from multiple sources.

####Project provides:
- Set of class which give you an simple example of sorting trips.
- PHPDoc.
- PHPUnit tests.
**Document checking completed. No errors or warnings to show.**

#### An installation guide.
1. Add PHP lib into your project and enjoy it.
2. Install a composer into main directory of the project. (`composer install` or `composer update`).
3. In order to test application run fallowing commands:
- `phpunit` - for phpunit test.

#### See working test below:
**[Test me live!](http://trip.sorter.gcoders.co.uk/)**