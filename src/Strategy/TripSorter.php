<?php

namespace Src\Strategy;

/**
 * Created by PhpStorm.
 * User: Marcin
 * Date: 29/04/2018
 * Time: 10:16
 */
class TripSorter
{
    public $trip;
    private $count;

    const DEPARTURE = 'Departure';
    const ARRIVAL   = 'Arrival';

    private $transportation = array(
        'bus'   => 'Src\Boarding\Bus',
        'train' => 'Src\Boarding\Train',
        'plane' => 'Src\Boarding\Plane'
    );

    public function __construct($trip)
    {
        $this->trip = $trip;
        $this->count = count($this->trip);
    }

    /**
     * Make a sort of the trip
     *
     * @return $this
     */
    public function sort()
    {
        $this->findStartStep();
        $this->findRestSteps();

        return $this;
    }

    /**
     * Create specific message for all types of transportation
     *
     * @return mixed
     */
    public function getTransportation()
    {
        foreach ($this->trip as $key => $trip) {
            $type = strtolower($trip['Transportation']);
            if (!isset($this->transportation[$type])){
                throw new Exception(
                    sprintf(
                        'Unsupported transportation : %s',
                        $type
                    )
                );
            }
            $this->trip[$key] = new $this->transportation[$type]($trip);
        }

        return $this->trip;
    }

    /**
     * Find a first step
     *
     * @return array
     */
    public function findStartStep()
    {
        for($i = 0; $i < $this->count; $i++) {
            $firstTrip = true;

            for($j = 0; $j < $this->count; $j++) {
                if($this->trip[$i][self::DEPARTURE] ==  $this->trip[$j][self::ARRIVAL]) {
                    $firstTrip = false;
                }
            }

            if($firstTrip) {
                array_unshift($this->trip, $this->trip[$i]);
                unset($this->trip[$i+1]);
                break;
            }
        }

        $this->trip = array_merge($this->trip);

        return $this->trip;
    }

    /**
     * Sort rest of the steps
     *
     * @return mixed
     */
    public function findRestSteps()
    {
        for($i = 0; $i < $this->count; $i++) {
            for($j = $i+1; $j < $this->count; $j++) {
                if($this->trip[$i][self::ARRIVAL] ==  $this->trip[$j][self::DEPARTURE]) {
                    $nextTrip = $i + 1;
                    $temp = $this->trip[$nextTrip];
                    $this->trip[$nextTrip] = $this->trip[$j];
                    $this->trip[$j] = $temp;
                    break;
                }
            }
        }

        return $this->trip;
    }


}