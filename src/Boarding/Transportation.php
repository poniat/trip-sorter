<?php

namespace Src\Boarding;

/**
 * Created by PhpStorm.
 * User: Marcin
 * Date: 29/04/2018
 * Time: 10:26
 */
abstract class Transportation implements TransportationInterface
{
    public $departure;

    public $arrival;

    const MESSAGE_FINAL_DESTINATION = 'You have arrived at your final destination.';

    /**
     * Transportation constructor.
     * @param array $trip
     */
    public function __construct(array $trip)
    {
        foreach ($trip as $key => $value) {
            $property = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $key))));
            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            }
        }

    }


}