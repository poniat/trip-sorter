<?php

namespace Src\Boarding;

/**
 * Created by PhpStorm.
 * User: Marcin
 * Date: 29/04/2018
 * Time: 10:20
 */
class Plane extends Transportation
{
    protected $transportationNumber;
    protected $seat;
    protected $gate;
    protected $baggage;

    const MESSAGE = 'From %s take flight %s to %s. Gate %s, seat %s.';
    const MESSAGE_BAGGAGE_TICKET = 'Baggage drop at ticket counter %s.';
    const MESSAGE_NO_BAGGAGE_TICKET = 'Baggage will we automatically transferred from your last leg.';

    /**
     * Create message for Plane transportation
     * @return string
     */
    public function getMessage()
    {
        $message = sprintf(
            static::MESSAGE,
            $this->departure,
            $this->transportationNumber,
            $this->arrival,
            $this->gate,
            $this->seat
        );

        if (!empty($this->baggage)){
            $message = sprintf($message . static::MESSAGE_BAGGAGE_TICKET, $this->baggage);
        } else {
            $message .= static::MESSAGE_NO_BAGGAGE_TICKET;
        }

        return $message;
    }

}