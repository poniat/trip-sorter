<?php

namespace Src\Boarding;

/**
 * Created by PhpStorm.
 * User: Marcin
 * Date: 29/04/2018
 * Time: 10:20
 */
class Train extends Transportation
{
    protected $transportationNumber;

    protected $seat;

    const MESSAGE = 'Take train %s';
    const MESSAGE_FROM_TO = ' from %s to %s. ';
    const MESSAGE_SEAT = 'Sit in seat %s.';

    /**
     * Create message for Train transportation
     * @return string
     */
    public function getMessage()
    {
        // Add Transportation number to the message
        $message = sprintf(static::MESSAGE, $this->transportationNumber);
        // Add (from => to) to the message
        $message = sprintf(
            $message . static::MESSAGE_FROM_TO,
            $this->departure,
            $this->arrival
        );
        // Add Seat number to the message
        $message = sprintf($message . static::MESSAGE_SEAT, $this->seat);
        return $message;
    }
}