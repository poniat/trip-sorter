<?php

namespace Src\Boarding;

/**
 * Created by PhpStorm.
 * User: Marcin
 * Date: 29/04/2018
 * Time: 10:20
 */
class Bus extends Transportation
{
    const MESSAGE = 'Take the airport bus';
    const MESSAGE_FROM_TO = ' from %s to %s.';
    const MESSAGE_NO_SEAT = 'No seat assignment.';

    /**
     * Create message for Bus transportation
     * @return string
     */
    public function getMessage()
    {
        $message = sprintf(
            static::MESSAGE . static::MESSAGE_FROM_TO,
            $this->departure,
            $this->arrival
        );
        $message .= static::MESSAGE_NO_SEAT;
        return $message;
    }

}