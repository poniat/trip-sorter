<?php

namespace Src\Reader;

use PHPUnit\Runner\Exception;

/**
 * Created by PhpStorm.
 * User: Marcin
 * Date: 29/04/2018
 * Time: 10:14
 */

class Reader
{
    /**
     * Read the trip from a json file
     *
     * @param $fileName
     * @return mixed
     */
    public static function fromFile($fileName)
    {
        if (!is_readable($fileName) || !$fileName) {
            throw new Exception('File %s is not readable', $fileName);
        }

        try {
            $content = file_get_contents($fileName);
            return self::decode($content, true);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Decode a json string to an array
     *
     * @param $json
     * @param $toArray
     * @return mixed
     */
    public static function decode($json, $toArray = Json::TYPE_ARRAY)
    {
        $decode = json_decode($json, $toArray);
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                break;
            case JSON_ERROR_DEPTH:
                throw new Exception('The maximum stack depth has been exceeded');
            case JSON_ERROR_STATE_MISMATCH:
                throw new Exception('Invalid or malformed JSON');
            case JSON_ERROR_CTRL_CHAR:
                throw new Exception('Control character error, possibly incorrectly encoded');
            case JSON_ERROR_SYNTAX:
                throw new Exception('Syntax error');
            case JSON_ERROR_UTF8:
                throw new Exception('Malformed UTF-8 characters, possibly incorrectly encoded');
            default:
                throw new Exception('Decoding failed');
        }

        return $decode;
    }

}