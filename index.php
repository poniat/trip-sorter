<?php
use Src\Reader\Reader;
use Src\Strategy\TripSorter;

require_once __DIR__ . '/vendor/autoload.php';

$path = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . 'trip.json';

$tripSteps = Reader::fromFile($path);
$trip = new TripSorter($tripSteps);
$trip = $trip->sort()->getTransportation();

$finalTrip = [];
if ($count = count($trip)) {
    foreach ($trip as $index => $step) {
        $finalTrip[] = $step->getMessage();
        if($index == $count - 1)
            $finalTrip[] = $step::MESSAGE_FINAL_DESTINATION;
    }
}

header('Content-Type: application/json');
echo json_encode($finalTrip);