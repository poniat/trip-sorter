<?php

use Src\Reader\Reader;

class ReaderTest extends \PHPUnit_Framework_TestCase
{
    public function testJsonReader()
    {
        echo $path = dirname(__DIR__) . DIRECTORY_SEPARATOR . '../upload' . DIRECTORY_SEPARATOR . 'trip.json';
        $data = Reader::fromFile($path);
        $this->assertTrue(is_array($data));
        $this->assertArrayHasKey('Departure', $data[0]);
        $this->assertArrayHasKey('Arrival', $data[0]);
        $this->assertArrayHasKey('Transportation', $data[0]);
        $this->assertArrayHasKey('Transportation_number', $data[0]);
        $this->assertArrayHasKey('Seat', $data[0]);
        $this->assertArrayHasKey('Gate', $data[0]);
        $this->assertArrayHasKey('Baggage', $data[0]);
    }

}