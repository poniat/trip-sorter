<?php

use Src\Strategy\TripSorter;

class TripSorterTest extends \PHPUnit_Framework_TestCase
{
    protected $sorter;

    protected $trip = array(
        array(
            'Departure'             => 'C',
            'Arrival'               => 'D',
            'Transportation'        => 'Bus',
        ),
        array(
            'Departure'             => 'A',
            'Arrival'               => 'B',
            'Transportation'        => 'Train',
        ),
        array(
            'Departure'             => 'B',
            'Arrival'               => 'C',
            'Transportation'        => 'Plane',
            'Transportation_number' => '2G',
            'Seat'                  => '51A',
            'Gate'                  => '13G',
        )
    );

    protected $expectedTrip = array(
        array(
            'Departure'             => 'A',
            'Arrival'               => 'B',
            'Transportation'        => 'Train',
        ),
        array(
            'Departure'             => 'B',
            'Arrival'               => 'C',
            'Transportation'        => 'Plane',
            'Transportation_number' => '2G',
            'Seat'                  => '51A',
            'Gate'                  => '13G',
        ),
        array(
            'Departure'             => 'C',
            'Arrival'               => 'D',
            'Transportation'        => 'Bus',
        ),
    );

    public function setUp(){
        $this->sorter = new TripSorter($this->trip);
    }

    public function testSortTrip()
    {
        $trip = $this->sorter->sort()->trip;
        $this->assertEquals($trip, $this->expectedTrip);
    }

    public function testTransportation()
    {
        $trip = $this->sorter->sort()->getTransportation();
        foreach ($trip as $step)
            $this->assertInstanceOf('Src\Boarding\Transportation', $step);
    }

}