<?php

use Src\Boarding\Train;

class TrainTest extends PHPUnit_Framework_TestCase
{
    protected $train;
    protected $trip = array(
        'Departure'      => 'New York',
        'Arrival'        => 'Dubaj',
        'Transportation' => 'Train',
        'Seat'           => '3B'
    );
    public function setUp()
    {
        $this->train = new Train($this->trip);
    }

    public function testGetMessage()
    {
        $message = $this->train->getMessage();
        $this->assertEquals(strlen($message), 51);
        $this->assertStringEndsWith($message, 'Take train  from New York to Dubaj. Sit in seat 3B.');
    }


}