<?php

use Src\Boarding\Plane;

class PlaneTest extends PHPUnit_Framework_TestCase
{
    protected $plane;
    protected $trip = array(
        'Departure'             => 'Liverpool',
        'Arrival'               => 'New York',
        'Transportation'        => 'Plane',
        'Transportation_number' => '32G',
        'Seat'                  => '6A',
        'Gate'                  => '42C',
    );
    public function setUp()
    {
        $this->plane = new Plane($this->trip);
    }

    public function testGetMessage()
    {
        $message = $this->plane->getMessage();
        $this->assertEquals(strlen($message), 123);
        $this->assertStringEndsWith($message, 'From Liverpool take flight 32G to New York. Gate 42C, seat 6A.Baggage will we automatically transferred from your last leg.');
    }


}