<?php

use Src\Boarding\Bus;

class SailorTest extends \PHPUnit_Framework_TestCase
{
    protected $bus;
    protected $trip = array(
        'Departure'      => 'Leeds',
        'Arrival'        => 'London',
        'Transportation' => 'Bus'
    );

    public function setUp(){
        $this->bus = new Bus($this->trip);
    }

    public function testGetMessage()
    {
        $message = $this->bus->getMessage();
        $this->assertEquals(strlen($message), 61);
        $this->assertStringEndsWith($message, 'Take the airport bus from Leeds to London.No seat assignment.');
    }


}